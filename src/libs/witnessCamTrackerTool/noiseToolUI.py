import random
from PySide2 import QtCore, QtWidgets as qw
from maya.api import OpenMayaAnim as omAnim, OpenMaya as om2


class SimpleNoiseWindow(qw.QMainWindow):
    """
    A basic PySide 2 UI for a noise baker Tool
    """
    def __init__(self):
        super(SimpleNoiseWindow,self).__init__()
        self.setGeometry(300,300,250,150)
        self.setWindowTitle("Noise Baker Window")
        self.buildUI()
        
    def buildUI(self):
        """
        build the UI elements here.  Arrange and lay them out with a vertical
        base and 2 horizontal sublayouts to control slider and spinbox Placement 
        """
        self.mainWidget = qw.QWidget()
        self.mainLayout = qw.QVBoxLayout()
        self.setCentralWidget(self.mainWidget)
        self.mainWidget.setLayout(self.mainLayout)
        
        self.freqWidget = qw.QWidget()
        self.freqLayout = qw.QHBoxLayout()
        self.freqLabel  = qw.QLabel('Frequency: ')
        self.freqSpinBox = qw.QDoubleSpinBox()
        self.freqRandSlider = qw.QSlider(QtCore.Qt.Horizontal)  
        
        self.freqRandSlider.setMinimum(0)
        self.freqRandSlider.setMaximum(10)
         
        self.freqLayout.addWidget(self.freqSpinBox)
        self.freqLayout.addWidget(self.freqRandSlider)     
        self.freqWidget.setLayout(self.freqLayout)
        self.mainLayout.addWidget(self.freqLabel)
        self.mainLayout.addWidget(self.freqWidget)        
        
        self.ampWidget = qw.QWidget()
        self.ampLayout = qw.QHBoxLayout()
        self.ampLabel  = qw.QLabel('Amplitude: ')
        
        self.ampSpinBox = qw.QDoubleSpinBox()
        self.ampRandSlider = qw.QSlider(QtCore.Qt.Horizontal)  
        
        self.ampRandSlider.setMinimum(0)
        self.ampRandSlider.setMaximum(10)
         
        self.ampLayout.addWidget(self.ampSpinBox)
        self.ampLayout.addWidget(self.ampRandSlider)     
        self.ampWidget.setLayout(self.ampLayout)
        self.mainLayout.addWidget(self.ampLabel)
        self.mainLayout.addWidget(self.ampWidget)        
        self.ampWidget.setLayout(self.ampLayout)
        
        self.applyButton = qw.QPushButton('Apply')
        self.mainLayout.addWidget(self.applyButton)
        self.applyButton.released.connect(self.runRandom)
        
    def runRandom(self):
        """
        print the values of the elements in the boxes and the time slider
        """
        freq = self.freqSpinBox.value()
        freqRandom = self.freqRandSlider.value()
        amp = self.ampSpinBox.value()
        ampRandom = self.ampRandSlider.value()
        animStart = omAnim.MAnimControl.animationStartTime().value
        animEnd = omAnim.MAnimControl.animationEndTime().value
        print freq, freqRandom, amp, ampRandom, animStart, animEnd
          
        
noiseWindow = SimpleNoiseWindow()
noiseWindow.show()