import maya.api.OpenMaya as om2

'''
Simple Noise Controller locator class, for basic randomly generated noise based on a  shader
'''

class CreateNoiseLocator:
    
    dgMod = om2.MDGModifier()
    
    def getMObjectFromName(self,name):
        """
        accessor to grab MObject Node's to wok with the maya API
        """
        selection = om2.MSelectionList()
        selection.add(name)
        return selection.getDependNode(0)    
    
    def build(self):
        """
        build the locator, shader and node, then connect them all accordingly
        """
        srcArray = om2.MPlugArray()
        noiseList = []
        mNoiseNode = om2.MFnDependencyNode()
        for x in xrange(2):
            noiseList.append(self.dgMod.createNode("noise"))
            mNoiseNode.setObject(noiseList[x])
            mNoiseNode.setName("noiseNode")
            mNoiseNode.findPlug("noiseType", True).setInt(x)
            srcArray.append(mNoiseNode.findPlug("outColorR", True))
        mLocatorNode = om2.MFnDagNode()
        mLocatorNode.create("locator")
        mLocatorNode.setName("noiseControllerNode")
        counter = 0
        for src in srcArray:
            tVal = "translateX"
            if not counter:
                tVal = "translateY"
            counter = counter + 1
            self.dgMod.connect(src, mLocatorNode.findPlug(tVal, True))
        self.timePlugFinder(noiseList)
        self.dgMod.doIt()
    
    def timePlugFinder(self, noiseList):
        """
        Access the time attribute on the noise node and connect it to the maya 
        global time node
        """
        mNoiseNode = om2.MFnDependencyNode()
        timeNode = om2.MFnDependencyNode()
        for node in noiseList:
            mNoiseNode.setObject(node)
            time = mNoiseNode.findPlug("time", True)
            timeNode.setObject(self.getMObjectFromName("time1"))
            self.dgMod.connect(timeNode.findPlug("outTime", True), time)

    def undoIt(self):
        self.dgMod.undoIt()      
        

cn = CreateNoiseLocator()
cn.build()
