import maya.cmds as cmds
import maya.api.OpenMaya as om2
import maya.api.OpenMayaAnim as om2Anim


"""
A basic method for determining post roll for all animCurves in a scene
"""
def postRollTool(postRollAmount = 30):
    
    #create a selection list and then access all anim curves
    selectionList = om2.MSelectionList()
    allCurves = cmds.ls(type = 'animCurve')
    for i, curves in enumerate(allCurves):
        #populate the selection list with all animation curves and grab their keyframes
        finalFrame = om2Anim.MAnimControl.maxTime().value
        curveName = curves.split('_')[0]
        curveAttr = curves.split('_')[-1]
        keyFrameTimes = cmds.keyframe(curveName, attribute = curveAttr, q = True)
        timeKeys = []
        if finalFrame < keyFrameTimes[-1]:
            timeKeys.extend(keyFrameTimes[2:-1])
        else:
            timeKeys.extend(keyFrameTimes[2:])           
        selectionList.add(curves)
        currentCurve = om2Anim.MFnAnimCurve(selectionList.getDependNode(i))
        startTime = om2.MTime(0.0, 6)
        
        #determine the next point to occur logically on a linear curve based on the last 2 points
        zeroVal = currentCurve.evaluate(startTime)
        lastKeyIndex = currentCurve.numKeys - 1
        secondLastKeyIndex = lastKeyIndex -1
        keyOneY = currentCurve.value(secondLastKeyIndex)
        keyOneX = timeKeys[0]
        keyTwoY = currentCurve.value(lastKeyIndex)
        keyTwoX = timeKeys[-1]
        finalFrame = finalFrame + postRollAmount
        finalVal = om2.MTime(finalFrame, 6)
        if keyOneY == 0 or keyTwoY == 0:
            currentCurve.addKey(finalVal, keyOneY)
            continue
        finalFrame = finalFrame + postRollAmount
        difference = (keyTwoY-keyOneY)/(keyTwoX-keyOneX)
        y = (difference * finalFrame) + zeroVal
        if (keyTwoY-keyOneY) == 0:
            Y = keyTwoY
            
        #Add the Key Values
        currentCurve.addKey(finalVal, y)
        
postRollTool(30)
    
    
        
